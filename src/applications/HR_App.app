<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>HR App</label>
    <tab>Asset_Management__c</tab>
    <tab>Position__c</tab>
    <tab>Candidate_Tracker__c</tab>
    <tab>Certification_Tracker__c</tab>
    <tab>Leave_Tracker</tab>
    <tab>Interview_Tracker__c</tab>
    <tab>Time_Entries</tab>
    <tab>Testsharing__c</tab>
    <tab>Time_Entry_Mobile_New</tab>
</CustomApplication>
